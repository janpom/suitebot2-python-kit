from typing import Iterable, Tuple

from suitebot2.game.player_move import PlayerMove


class GameRound:
    def __init__(self, moves: Iterable[PlayerMove]):
        self._moves = tuple(moves)

    def get_moves(self) -> Tuple[PlayerMove]:
        return self._moves
