from typing import NamedTuple

PlayerMove = NamedTuple('PlayerMove', [('player_id', int), ('move', str)])
