from suitebot2 import json_util
from suitebot2.ai.bot_ai import BotAi
from suitebot2.server.simple_request_handler import SimpleRequestHandler


class BotRequestHandler(SimpleRequestHandler):
    def __init__(self, bot_ai: BotAi) -> None:
        self._bot_ai = bot_ai

    def process_request(self, request: str) -> str:
        try:
            return self._process_request_internal(request)
        except Exception as e:
            print(e)
            return str(e)

    def _process_request_internal(self, request: str) -> str:
        return self._process_move_request(request)

    def _process_move_request(self, request: str) -> str:
        message_type = json_util.decode_message_type(request)
        if message_type == "SETUP":
            return self._bot_ai.initialize_and_make_move(json_util.decode_setup_message(request))
        elif message_type == "MOVES":
            return self._bot_ai.make_move(json_util.decode_moves_message(request))
        else:
            raise ValueError("unexpected request: %s" % request)
